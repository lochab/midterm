/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * @author Yash Lochab
 */
package inventory;

import static inventory.Item.inventory;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Inventory {
    ArrayList<Item> itemList;
    
    Inventory(){
        itemList=new ArrayList<>();
        printInventory();
    }
    
    public void printInventory() {
        for (int i = 0; i < itemList.size(); i++) {
            System.out.println("ID: " + itemList.get(i).itemID
                    + "\t Name: " + itemList.get(i).getName()
                    + "\t Quantity:" + inventory[i].getItemQuantity(itemList.get(i).itemID));
        }
    }
}
